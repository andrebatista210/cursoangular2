import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CursosComponent } from './cursos.component';
import { CursosDetalheComponent } from './cursos-detalhe/cursos-detalhe.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CursosComponent, 
    CursosDetalheComponent
  ],
  exports: [
    CursosComponent,
    //CursosDetalheComponent ao não declarar aqui ele não fica visivel para appModules
  ]
})
export class CursosModule { }
