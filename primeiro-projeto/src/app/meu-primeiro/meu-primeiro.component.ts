/**
 * Created by Andre on 02/07/2017.
 */
import { Component } from '@angular/core';

@Component({
    selector: 'meu-primeiro-component',
    template: `
    <H1>Meu primeiro component com Angular 2!</H1>
    `
})
export class MeuPrimeiroComponent{}