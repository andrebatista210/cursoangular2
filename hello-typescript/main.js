var minhaVar = 'Minha variavel';
function myFunction(x, y) {
    return x + y;
}
//ECMASCRIPT 6 OU ECMASCRIPT 2015
var num = 2;
var PI = 3.14;
var num1 = [1, 2, 3, 4, 5];
num1.map(function (valore) {
    return valore * 2;
});
//com arrows functions =>
var numbers = [1, 2, 3, 4, 5];
numbers.map(function (v) { return v + 1; });
//ES 6
var Match = (function () {
    function Match() {
    }
    Match.prototype.soma = function (x, y) {
        x + y;
    };
    return Match;
}());
//JS
var n1 = 'asjadlkjssd';
n1 = 1;
//EM6 tem como tipar as variaveis
var n2 = 'asjadlkjssd';
n2 = 1;
