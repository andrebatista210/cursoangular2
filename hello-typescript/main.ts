var minhaVar = 'Minha variavel';

function myFunction(x,y){
  
    return x+y;
}
//ECMASCRIPT 6 OU ECMASCRIPT 2015
let num = 2;
const PI = 3.14;

var num1 = [1,2,3,4,5];
num1.map(function(valore){
    return valore * 2;
});

//com arrows functions =>
let numbers = [1,2,3,4,5];
numbers.map((v)=> v+1);

//ES 6
class Match{
    soma(x,y){
        x+y;
    }
}

//JS

var n1 = 'asjadlkjssd';
n1 = 1;

//EM6 tem como tipar as variaveis

var n2 :string = 'asjadlkjssd';
n2 = 1;
